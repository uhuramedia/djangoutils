# djangoutils

Suppose you have email service like Mailjet for your production. Normally, the Error emails
is sent through email service you set up on the production. If your application generate any error logs, it will
go through the mailjet service. Sometime mailjet blocks those error emails. To keep clean, the error is handled by different
email service.


### Installation
    
    $ pip install git+https://gitlab.com/uhuramedia/djangoutils.git
    
### Configuration

To make it work you need to add in `handlers` -> `mail_admins`, add in class `djangoutils.log.CustomAdminEmailHandler` in your 
`production.py` file 
    
    LOGFILE_ROOT = join(dirname(BASE_DIR), 'logs')
    LOGGING = {
        'version': 1,
        'disable_existing_loggers': True,
        'filters': {
            'require_debug_false': {
                '()': 'django.utils.log.RequireDebugFalse'
            }
        },
        'formatters': {
            'verbose': {
                'format': '%(levelname)s %(asctime)s %(module)s %(process)d %(thread)d %(message)s'
            },
            'simple': {
                'format': '%(levelname)s %(message)s'
            },
        },
        'handlers': {
            'mail_admins': {
                'level': 'ERROR',
                'filters': ['require_debug_false'],
                'class': 'djangoutils.log.CustomAdminEmailHandler'
            },
            'file': {
                'level': 'DEBUG',
                'class': 'logging.FileHandler',
                'filename': '{0}/project.log'.format(LOGFILE_ROOT),
                'formatter': 'verbose'
            },
        },
        'loggers': {
            'django.request': {
                'handlers': ['mail_admins'],
                'level': 'ERROR',
                'propagate': True,
            },
            'app_debug': {
                'handlers': ['file'],
                'level': 'DEBUG',
                'propagate': True,
            },
        }
    }

Then add simply in `production.py` the sample below and change the configuration accordingly

    MT_EMAIL_HOST = 'smpt.example.io'
    MT_EMAIL_HOST_USER = 'xxxxxx'
    MT_EMAIL_HOST_PASSWORD = 'xxxx'
    MT_EMAIL_PORT = '2525'
    SERVER_EMAIL = 'Live example <dev@example.com>'